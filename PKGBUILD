# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

pkgname=js102
pkgver=102.2.0
pkgrel=0.1
pkgdesc="JavaScript interpreter and libraries - Version 102"
arch=(x86_64 aarch64)
url="https://spidermonkey.dev/"
license=(MPL)
depends=(gcc-libs readline zlib sh)
makedepends=(zip autoconf2.13 python-setuptools python-psutil rust llvm clang lld)
checkdepends=(mercurial git)
options=(!lto debug)
_relver=${pkgver}esr
source=(https://archive.mozilla.org/pub/firefox/releases/$_relver/source/firefox-$_relver.source.tar.xz{,.asc})
sha256sums=('014d91d14ab4f53e93728273b45ac6022813d5ade35f842e722bf87b747c97ff'
            'SKIP')
validpgpkeys=('14F26682D0916CDD81E37B6D61B7B526D98F0353') # Mozilla Software Releases <release@mozilla.com>

# Make sure the duplication between bin and lib is found
COMPRESSZST+=(--long)

prepare() {
  mkdir mozbuild
  cd firefox-$pkgver

  # pull in the updated packages.
  cargo vendor third_party/rust

  cat >../mozconfig <<END
ac_add_options --enable-application=js
mk_add_options MOZ_OBJDIR=${PWD@Q}/obj

ac_add_options --prefix=/usr
ac_add_options --enable-release
ac_add_options --disable-lto
ac_add_options --enable-hardening
ac_add_options --enable-optimize
ac_add_options --disable-rust-simd
ac_add_options --enable-linker=lld
ac_add_options --disable-bootstrap
ac_add_options --disable-debug
ac_add_options --disable-debug-symbols
ac_add_options --disable-jemalloc
ac_add_options --disable-strip

# System libraries
ac_add_options --with-system-zlib
ac_add_options --without-system-icu

# Features
ac_add_options --enable-readline
ac_add_options --enable-shared-js
ac_add_options --enable-tests
ac_add_options --with-intl-api
END

  echo 'ac_add_options --enable-optimize="-g0 -O2"' >> ..mozconfig
  echo "mk_add_options MOZ_MAKE_FLAGS=\"${MAKEFLAGS}\"" >> ..mozconfig

  export MOZ_DEBUG_FLAGS=" "
  export CFLAGS+=" -g0"
  export CXXFLAGS+=" -g0"
  export DEBUG_CFLAGS="-g"
  export DEBUG_CXXFLAGS="-g"
  export LDFLAGS+=" -Wl,--no-keep-memory"
  export RUSTFLAGS="-Cdebuginfo=0"
}

build() {
  cd firefox-$pkgver

  export MOZ_NOSPAM=1
  export MOZBUILD_STATE_PATH="$srcdir/mozbuild"
  export MACH_USE_SYSTEM_PYTHON=1

  # Do 3-tier PGO
  echo "Building instrumented JS..."
  cat >.mozconfig ../mozconfig - <<END
ac_add_options --enable-profile-generate=cross
END
  ./mach build

  echo "Profiling instrumented JS..."
  (
    local js="$PWD/obj/dist/bin/js"
    export LLVM_PROFILE_FILE="$PWD/js-%p-%m.profraw"

    cd js/src/octane
    "$js" run.js

    cd ../../../third_party/webkit/PerformanceTests/ARES-6
    "$js" cli.js

    cd ../SunSpider/sunspider-0.9.1
    "$js" sunspider-standalone-driver.js
  )

  llvm-profdata merge -o merged.profdata *.profraw

  stat -c "Profile data found (%s bytes)" merged.profdata
  test -s merged.profdata

  echo "Removing instrumented JS..."
  ./mach clobber

  echo "Building optimized JS..."
  cat >.mozconfig ../mozconfig - <<END
ac_add_options --enable-lto=cross
ac_add_options --enable-profile-use=cross
ac_add_options --with-pgo-profile-path=${PWD@Q}/merged.profdata
END
  ./mach build
}

check() {
  local jstests_extra_args=(
    --format=none
    --exclude-random
    --wpt=disabled
  ) jittest_extra_args=(
    --format=none
    --timeout 300
  ) jittest_test_args=(
    basic
  )

  cd firefox-$pkgver/obj
  make -C js/src check-jstests check-jit-test \
    JSTESTS_EXTRA_ARGS="${jstests_extra_args[*]}" \
    JITTEST_EXTRA_ARGS="${jittest_extra_args[*]}" \
    JITTEST_TEST_ARGS="${jittest_test_args[*]}"
}

package() {
  cd firefox-$pkgver/obj
  make DESTDIR="$pkgdir" install
  rm "$pkgdir"/usr/lib/*.ajs
  find "$pkgdir"/usr/{lib/pkgconfig,include} -type f -exec chmod -c a-x {} +
}

# vim:set sw=2 et:
